#!/bin/bash
CASOS=(15 20 30 40 50)
for index in ${CASOS[@]}; do 
    for caso in casos/*${index}.pl; do
        sbatch --output=${caso}.out scriptSub.sh ${caso}
    done
done
