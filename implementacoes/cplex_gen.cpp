#include <iostream>

using namespace std;
#define MAXN 1000
int main()
{
    int N, M;
    int D[MAXN][MAXN];

    cin >> N >> M;

    for(int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            cin >> D[i][j];
        }
    }
    cout << "enter pdm" << endl
         << "Maximize" << endl << "[ ";

    for (int i = 0; i < N - 1; ++i) {
	if (i > 0)
		cout << " + ";

        cout << D[i][i + 1] << " x" << i << " * x" << i + 1;
        for (int j = i + 2; j < N; ++j) {
            cout << " + " << D[i][j] << " x" << i << " * x" << j << " ";
        }
    }

    cout << "] / 2" << endl << "Subject To" << endl;

    for (int i = 0; i < N - 1; ++i) {
        cout << "x" <<  i << " + ";
    }

    cout << "x" << N - 1 << " = " << M << endl;
    
    cout << "binary" << endl;

    for (int i = 0; i < N; ++i)
        cout << " x" << i;

    cout << endl << "End" << endl
         << "optimize" << endl
         << "display solution variables x0-x" << N - 1 << endl
         << "display solution objective" << endl;  
    return 0;
}

