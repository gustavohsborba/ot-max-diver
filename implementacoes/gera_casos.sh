#!/bin/bash

if [ -d casos ]; then
	rm -rf casos/
fi

mkdir casos

SIZES=(10 15 20 30 40 50)

for size in ${SIZES[@]}; do
	for i in $(seq 1 10); do
		./generator ${size}  > casos/teste_${i}_${size}.in
		./model < casos/teste_${i}_${size}.in > casos/model_${i}_${size}.pl
        sleep 1
	done
done
