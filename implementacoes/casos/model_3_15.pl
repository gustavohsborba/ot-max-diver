enter pdm
Maximize
[ 61 x0 * x1 + 90 x0 * x2  + 92 x0 * x3  + 14 x0 * x4  + 15 x0 * x5  + 95 x0 * x6  + 19 x0 * x7  + 51 x0 * x8  + 43 x0 * x9  + 36 x0 * x10  + 68 x0 * x11  + 18 x0 * x12  + 75 x0 * x13  + 87 x0 * x14  + 53 x1 * x2 + 59 x1 * x3  + 30 x1 * x4  + 15 x1 * x5  + 23 x1 * x6  + 31 x1 * x7  + 96 x1 * x8  + 67 x1 * x9  + 71 x1 * x10  + 24 x1 * x11  + 31 x1 * x12  + 72 x1 * x13  + 61 x1 * x14  + 38 x2 * x3 + 62 x2 * x4  + 99 x2 * x5  + 36 x2 * x6  + 32 x2 * x7  + 2 x2 * x8  + 25 x2 * x9  + 83 x2 * x10  + 44 x2 * x11  + 7 x2 * x12  + 99 x2 * x13  + 57 x2 * x14  + 9 x3 * x4 + 70 x3 * x5  + 92 x3 * x6  + 36 x3 * x7  + 11 x3 * x8  + 57 x3 * x9  + 24 x3 * x10  + 81 x3 * x11  + 42 x3 * x12  + 57 x3 * x13  + 62 x3 * x14  + 14 x4 * x5 + 49 x4 * x6  + 11 x4 * x7  + 85 x4 * x8  + 27 x4 * x9  + 15 x4 * x10  + 74 x4 * x11  + 39 x4 * x12  + 95 x4 * x13  + 1 x4 * x14  + 1 x5 * x6 + 38 x5 * x7  + 6 x5 * x8  + 39 x5 * x9  + 83 x5 * x10  + 39 x5 * x11  + 86 x5 * x12  + 31 x5 * x13  + 97 x5 * x14  + 62 x6 * x7 + 54 x6 * x8  + 23 x6 * x9  + 24 x6 * x10  + 83 x6 * x11  + 32 x6 * x12  + 70 x6 * x13  + 31 x6 * x14  + 43 x7 * x8 + 77 x7 * x9  + 33 x7 * x10  + 36 x7 * x11  + 61 x7 * x12  + 1 x7 * x13  + 83 x7 * x14  + 9 x8 * x9 + 14 x8 * x10  + 75 x8 * x11  + 62 x8 * x12  + 99 x8 * x13  + 56 x8 * x14  + 7 x9 * x10 + 88 x9 * x11  + 92 x9 * x12  + 45 x9 * x13  + 24 x9 * x14  + 41 x10 * x11 + 49 x10 * x12  + 97 x10 * x13  + 21 x10 * x14  + 85 x11 * x12 + 79 x11 * x13  + 41 x11 * x14  + 70 x12 * x13 + 55 x12 * x14  + 81 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 6
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
