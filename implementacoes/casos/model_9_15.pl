enter pdm
Maximize
[ 18 x0 * x1 + 71 x0 * x2  + 97 x0 * x3  + 79 x0 * x4  + 82 x0 * x5  + 29 x0 * x6  + 57 x0 * x7  + 67 x0 * x8  + 49 x0 * x9  + 22 x0 * x10  + 70 x0 * x11  + 74 x0 * x12  + 85 x0 * x13  + 22 x0 * x14  + 3 x1 * x2 + 89 x1 * x3  + 54 x1 * x4  + 12 x1 * x5  + 46 x1 * x6  + 58 x1 * x7  + 54 x1 * x8  + 59 x1 * x9  + 72 x1 * x10  + 48 x1 * x11  + 28 x1 * x12  + 52 x1 * x13  + 88 x1 * x14  + 8 x2 * x3 + 12 x2 * x4  + 39 x2 * x5  + 43 x2 * x6  + 26 x2 * x7  + 35 x2 * x8  + 69 x2 * x9  + 43 x2 * x10  + 27 x2 * x11  + 42 x2 * x12  + 40 x2 * x13  + 86 x2 * x14  + 96 x3 * x4 + 48 x3 * x5  + 3 x3 * x6  + 36 x3 * x7  + 85 x3 * x8  + 12 x3 * x9  + 64 x3 * x10  + 1 x3 * x11  + 43 x3 * x12  + 75 x3 * x13  + 80 x3 * x14  + 47 x4 * x5 + 45 x4 * x6  + 1 x4 * x7  + 76 x4 * x8  + 54 x4 * x9  + 27 x4 * x10  + 15 x4 * x11  + 40 x4 * x12  + 19 x4 * x13  + 15 x4 * x14  + 1 x5 * x6 + 35 x5 * x7  + 89 x5 * x8  + 51 x5 * x9  + 39 x5 * x10  + 32 x5 * x11  + 47 x5 * x12  + 75 x5 * x13  + 17 x5 * x14  + 32 x6 * x7 + 81 x6 * x8  + 76 x6 * x9  + 16 x6 * x10  + 88 x6 * x11  + 93 x6 * x12  + 23 x6 * x13  + 54 x6 * x14  + 16 x7 * x8 + 92 x7 * x9  + 35 x7 * x10  + 51 x7 * x11  + 15 x7 * x12  + 12 x7 * x13  + 95 x7 * x14  + 98 x8 * x9 + 96 x8 * x10  + 59 x8 * x11  + 91 x8 * x12  + 96 x8 * x13  + 58 x8 * x14  + 41 x9 * x10 + 28 x9 * x11  + 42 x9 * x12  + 70 x9 * x13  + 48 x9 * x14  + 31 x10 * x11 + 88 x10 * x12  + 38 x10 * x13  + 34 x10 * x14  + 62 x11 * x12 + 49 x11 * x13  + 4 x11 * x14  + 36 x12 * x13 + 92 x12 * x14  + 100 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 6
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
