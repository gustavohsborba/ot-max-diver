enter pdm
Maximize
[ 60 x0 * x1 + 61 x0 * x2  + 74 x0 * x3  + 49 x0 * x4  + 91 x0 * x5  + 77 x0 * x6  + 38 x0 * x7  + 42 x0 * x8  + 44 x0 * x9  + 15 x1 * x2 + 55 x1 * x3  + 39 x1 * x4  + 49 x1 * x5  + 65 x1 * x6  + 80 x1 * x7  + 67 x1 * x8  + 27 x1 * x9  + 17 x2 * x3 + 57 x2 * x4  + 98 x2 * x5  + 36 x2 * x6  + 15 x2 * x7  + 62 x2 * x8  + 72 x2 * x9  + 21 x3 * x4 + 15 x3 * x5  + 83 x3 * x6  + 69 x3 * x7  + 1 x3 * x8  + 53 x3 * x9  + 81 x4 * x5 + 19 x4 * x6  + 94 x4 * x7  + 27 x4 * x8  + 47 x4 * x9  + 85 x5 * x6 + 89 x5 * x7  + 28 x5 * x8  + 63 x5 * x9  + 29 x6 * x7 + 26 x6 * x8  + 53 x6 * x9  + 82 x7 * x8 + 96 x7 * x9  + 29 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 4
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
