enter pdm
Maximize
[ 63 x0 * x1 + 32 x0 * x2  + 99 x0 * x3  + 51 x0 * x4  + 15 x0 * x5  + 90 x0 * x6  + 10 x0 * x7  + 42 x0 * x8  + 64 x0 * x9  + 100 x1 * x2 + 39 x1 * x3  + 60 x1 * x4  + 66 x1 * x5  + 10 x1 * x6  + 59 x1 * x7  + 54 x1 * x8  + 56 x1 * x9  + 62 x2 * x3 + 69 x2 * x4  + 24 x2 * x5  + 10 x2 * x6  + 36 x2 * x7  + 52 x2 * x8  + 87 x2 * x9  + 95 x3 * x4 + 14 x3 * x5  + 61 x3 * x6  + 52 x3 * x7  + 4 x3 * x8  + 84 x3 * x9  + 98 x4 * x5 + 35 x4 * x6  + 61 x4 * x7  + 37 x4 * x8  + 50 x4 * x9  + 2 x5 * x6 + 66 x5 * x7  + 51 x5 * x8  + 53 x5 * x9  + 62 x6 * x7 + 54 x6 * x8  + 49 x6 * x9  + 28 x7 * x8 + 73 x7 * x9  + 66 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 5
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
