enter pdm
Maximize
[ 36 x0 * x1 + 22 x0 * x2  + 23 x0 * x3  + 75 x0 * x4  + 12 x0 * x5  + 80 x0 * x6  + 31 x0 * x7  + 6 x0 * x8  + 53 x0 * x9  + 15 x0 * x10  + 8 x0 * x11  + 33 x0 * x12  + 16 x0 * x13  + 91 x0 * x14  + 12 x1 * x2 + 27 x1 * x3  + 1 x1 * x4  + 80 x1 * x5  + 66 x1 * x6  + 3 x1 * x7  + 76 x1 * x8  + 89 x1 * x9  + 40 x1 * x10  + 77 x1 * x11  + 65 x1 * x12  + 99 x1 * x13  + 10 x1 * x14  + 27 x2 * x3 + 49 x2 * x4  + 34 x2 * x5  + 18 x2 * x6  + 39 x2 * x7  + 63 x2 * x8  + 77 x2 * x9  + 91 x2 * x10  + 81 x2 * x11  + 77 x2 * x12  + 4 x2 * x13  + 68 x2 * x14  + 35 x3 * x4 + 86 x3 * x5  + 67 x3 * x6  + 69 x3 * x7  + 41 x3 * x8  + 1 x3 * x9  + 84 x3 * x10  + 73 x3 * x11  + 61 x3 * x12  + 23 x3 * x13  + 100 x3 * x14  + 83 x4 * x5 + 65 x4 * x6  + 16 x4 * x7  + 50 x4 * x8  + 23 x4 * x9  + 6 x4 * x10  + 82 x4 * x11  + 18 x4 * x12  + 56 x4 * x13  + 22 x4 * x14  + 23 x5 * x6 + 34 x5 * x7  + 27 x5 * x8  + 88 x5 * x9  + 56 x5 * x10  + 9 x5 * x11  + 52 x5 * x12  + 44 x5 * x13  + 56 x5 * x14  + 65 x6 * x7 + 63 x6 * x8  + 81 x6 * x9  + 6 x6 * x10  + 35 x6 * x11  + 1 x6 * x12  + 85 x6 * x13  + 85 x6 * x14  + 76 x7 * x8 + 9 x7 * x9  + 88 x7 * x10  + 74 x7 * x11  + 50 x7 * x12  + 51 x7 * x13  + 39 x7 * x14  + 74 x8 * x9 + 10 x8 * x10  + 58 x8 * x11  + 60 x8 * x12  + 51 x8 * x13  + 85 x8 * x14  + 96 x9 * x10 + 13 x9 * x11  + 26 x9 * x12  + 61 x9 * x13  + 45 x9 * x14  + 89 x10 * x11 + 64 x10 * x12  + 83 x10 * x13  + 56 x10 * x14  + 99 x11 * x12 + 23 x11 * x13  + 36 x11 * x14  + 95 x12 * x13 + 97 x12 * x14  + 5 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 4
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
