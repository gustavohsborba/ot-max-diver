enter pdm
Maximize
[ 32 x0 * x1 + 54 x0 * x2  + 40 x0 * x3  + 96 x0 * x4  + 8 x0 * x5  + 57 x0 * x6  + 7 x0 * x7  + 98 x0 * x8  + 100 x0 * x9  + 26 x1 * x2 + 35 x1 * x3  + 50 x1 * x4  + 20 x1 * x5  + 48 x1 * x6  + 47 x1 * x7  + 17 x1 * x8  + 16 x1 * x9  + 47 x2 * x3 + 20 x2 * x4  + 3 x2 * x5  + 31 x2 * x6  + 17 x2 * x7  + 77 x2 * x8  + 58 x2 * x9  + 70 x3 * x4 + 52 x3 * x5  + 13 x3 * x6  + 29 x3 * x7  + 81 x3 * x8  + 19 x3 * x9  + 4 x4 * x5 + 40 x4 * x6  + 87 x4 * x7  + 70 x4 * x8  + 86 x4 * x9  + 96 x5 * x6 + 97 x5 * x7  + 2 x5 * x8  + 65 x5 * x9  + 67 x6 * x7 + 21 x6 * x8  + 90 x6 * x9  + 56 x7 * x8 + 88 x7 * x9  + 17 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 4
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
