enter pdm
Maximize
[ 64 x0 * x1 + 55 x0 * x2  + 63 x0 * x3  + 18 x0 * x4  + 21 x0 * x5  + 58 x0 * x6  + 61 x0 * x7  + 8 x0 * x8  + 46 x0 * x9  + 33 x0 * x10  + 100 x0 * x11  + 14 x0 * x12  + 36 x0 * x13  + 63 x0 * x14  + 19 x1 * x2 + 91 x1 * x3  + 21 x1 * x4  + 26 x1 * x5  + 41 x1 * x6  + 61 x1 * x7  + 21 x1 * x8  + 55 x1 * x9  + 69 x1 * x10  + 51 x1 * x11  + 72 x1 * x12  + 97 x1 * x13  + 71 x1 * x14  + 13 x2 * x3 + 20 x2 * x4  + 87 x2 * x5  + 95 x2 * x6  + 31 x2 * x7  + 64 x2 * x8  + 37 x2 * x9  + 34 x2 * x10  + 3 x2 * x11  + 73 x2 * x12  + 82 x2 * x13  + 2 x2 * x14  + 67 x3 * x4 + 33 x3 * x5  + 85 x3 * x6  + 34 x3 * x7  + 72 x3 * x8  + 17 x3 * x9  + 80 x3 * x10  + 79 x3 * x11  + 2 x3 * x12  + 67 x3 * x13  + 11 x3 * x14  + 40 x4 * x5 + 98 x4 * x6  + 60 x4 * x7  + 75 x4 * x8  + 74 x4 * x9  + 54 x4 * x10  + 58 x4 * x11  + 40 x4 * x12  + 77 x4 * x13  + 97 x4 * x14  + 50 x5 * x6 + 93 x5 * x7  + 34 x5 * x8  + 57 x5 * x9  + 31 x5 * x10  + 23 x5 * x11  + 98 x5 * x12  + 48 x5 * x13  + 71 x5 * x14  + 80 x6 * x7 + 86 x6 * x8  + 94 x6 * x9  + 29 x6 * x10  + 94 x6 * x11  + 11 x6 * x12  + 10 x6 * x13  + 76 x6 * x14  + 65 x7 * x8 + 60 x7 * x9  + 14 x7 * x10  + 81 x7 * x11  + 33 x7 * x12  + 87 x7 * x13  + 21 x7 * x14  + 41 x8 * x9 + 92 x8 * x10  + 49 x8 * x11  + 57 x8 * x12  + 47 x8 * x13  + 95 x8 * x14  + 11 x9 * x10 + 27 x9 * x11  + 3 x9 * x12  + 60 x9 * x13  + 30 x9 * x14  + 18 x10 * x11 + 18 x10 * x12  + 89 x10 * x13  + 60 x10 * x14  + 77 x11 * x12 + 77 x11 * x13  + 92 x11 * x14  + 69 x12 * x13 + 92 x12 * x14  + 45 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 9
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
