enter pdm
Maximize
[ 92 x0 * x1 + 47 x0 * x2  + 21 x0 * x3  + 83 x0 * x4  + 8 x0 * x5  + 90 x0 * x6  + 35 x0 * x7  + 41 x0 * x8  + 11 x0 * x9  + 91 x0 * x10  + 72 x0 * x11  + 38 x0 * x12  + 84 x0 * x13  + 35 x0 * x14  + 8 x1 * x2 + 20 x1 * x3  + 86 x1 * x4  + 61 x1 * x5  + 44 x1 * x6  + 75 x1 * x7  + 78 x1 * x8  + 86 x1 * x9  + 54 x1 * x10  + 21 x1 * x11  + 67 x1 * x12  + 10 x1 * x13  + 63 x1 * x14  + 76 x2 * x3 + 18 x2 * x4  + 33 x2 * x5  + 87 x2 * x6  + 47 x2 * x7  + 29 x2 * x8  + 33 x2 * x9  + 8 x2 * x10  + 34 x2 * x11  + 84 x2 * x12  + 62 x2 * x13  + 9 x2 * x14  + 43 x3 * x4 + 74 x3 * x5  + 77 x3 * x6  + 56 x3 * x7  + 84 x3 * x8  + 80 x3 * x9  + 30 x3 * x10  + 42 x3 * x11  + 22 x3 * x12  + 45 x3 * x13  + 25 x3 * x14  + 52 x4 * x5 + 2 x4 * x6  + 34 x4 * x7  + 76 x4 * x8  + 80 x4 * x9  + 30 x4 * x10  + 61 x4 * x11  + 47 x4 * x12  + 50 x4 * x13  + 19 x4 * x14  + 60 x5 * x6 + 48 x5 * x7  + 36 x5 * x8  + 40 x5 * x9  + 62 x5 * x10  + 63 x5 * x11  + 16 x5 * x12  + 30 x5 * x13  + 7 x5 * x14  + 19 x6 * x7 + 4 x6 * x8  + 40 x6 * x9  + 41 x6 * x10  + 70 x6 * x11  + 61 x6 * x12  + 80 x6 * x13  + 14 x6 * x14  + 48 x7 * x8 + 13 x7 * x9  + 16 x7 * x10  + 44 x7 * x11  + 38 x7 * x12  + 36 x7 * x13  + 37 x7 * x14  + 13 x8 * x9 + 36 x8 * x10  + 91 x8 * x11  + 28 x8 * x12  + 53 x8 * x13  + 43 x8 * x14  + 87 x9 * x10 + 5 x9 * x11  + 25 x9 * x12  + 1 x9 * x13  + 35 x9 * x14  + 47 x10 * x11 + 29 x10 * x12  + 21 x10 * x13  + 35 x10 * x14  + 81 x11 * x12 + 94 x11 * x13  + 58 x11 * x14  + 13 x12 * x13 + 47 x12 * x14  + 73 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 6
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
