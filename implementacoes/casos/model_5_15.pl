enter pdm
Maximize
[ 52 x0 * x1 + 51 x0 * x2  + 50 x0 * x3  + 27 x0 * x4  + 40 x0 * x5  + 47 x0 * x6  + 6 x0 * x7  + 62 x0 * x8  + 68 x0 * x9  + 14 x0 * x10  + 7 x0 * x11  + 58 x0 * x12  + 63 x0 * x13  + 61 x0 * x14  + 32 x1 * x2 + 7 x1 * x3  + 4 x1 * x4  + 34 x1 * x5  + 85 x1 * x6  + 56 x1 * x7  + 81 x1 * x8  + 92 x1 * x9  + 44 x1 * x10  + 98 x1 * x11  + 27 x1 * x12  + 98 x1 * x13  + 23 x1 * x14  + 2 x2 * x3 + 11 x2 * x4  + 71 x2 * x5  + 50 x2 * x6  + 85 x2 * x7  + 83 x2 * x8  + 92 x2 * x9  + 82 x2 * x10  + 8 x2 * x11  + 81 x2 * x12  + 28 x2 * x13  + 38 x2 * x14  + 84 x3 * x4 + 63 x3 * x5  + 32 x3 * x6  + 23 x3 * x7  + 65 x3 * x8  + 78 x3 * x9  + 63 x3 * x10  + 60 x3 * x11  + 50 x3 * x12  + 57 x3 * x13  + 30 x3 * x14  + 51 x4 * x5 + 62 x4 * x6  + 96 x4 * x7  + 31 x4 * x8  + 76 x4 * x9  + 28 x4 * x10  + 59 x4 * x11  + 56 x4 * x12  + 88 x4 * x13  + 34 x4 * x14  + 77 x5 * x6 + 81 x5 * x7  + 66 x5 * x8  + 83 x5 * x9  + 43 x5 * x10  + 88 x5 * x11  + 56 x5 * x12  + 60 x5 * x13  + 27 x5 * x14  + 54 x6 * x7 + 14 x6 * x8  + 63 x6 * x9  + 91 x6 * x10  + 94 x6 * x11  + 32 x6 * x12  + 7 x6 * x13  + 39 x6 * x14  + 37 x7 * x8 + 98 x7 * x9  + 33 x7 * x10  + 24 x7 * x11  + 19 x7 * x12  + 15 x7 * x13  + 12 x7 * x14  + 46 x8 * x9 + 51 x8 * x10  + 70 x8 * x11  + 5 x8 * x12  + 66 x8 * x13  + 59 x8 * x14  + 75 x9 * x10 + 60 x9 * x11  + 29 x9 * x12  + 5 x9 * x13  + 40 x9 * x14  + 89 x10 * x11 + 84 x10 * x12  + 22 x10 * x13  + 19 x10 * x14  + 49 x11 * x12 + 77 x11 * x13  + 14 x11 * x14  + 15 x12 * x13 + 24 x12 * x14  + 89 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 4
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
