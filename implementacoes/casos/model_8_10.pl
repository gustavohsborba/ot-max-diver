enter pdm
Maximize
[ 70 x0 * x1 + 5 x0 * x2  + 1 x0 * x3  + 73 x0 * x4  + 70 x0 * x5  + 77 x0 * x6  + 37 x0 * x7  + 31 x0 * x8  + 31 x0 * x9  + 81 x1 * x2 + 47 x1 * x3  + 83 x1 * x4  + 73 x1 * x5  + 47 x1 * x6  + 39 x1 * x7  + 82 x1 * x8  + 76 x1 * x9  + 65 x2 * x3 + 85 x2 * x4  + 59 x2 * x5  + 95 x2 * x6  + 60 x2 * x7  + 82 x2 * x8  + 67 x2 * x9  + 56 x3 * x4 + 55 x3 * x5  + 25 x3 * x6  + 97 x3 * x7  + 52 x3 * x8  + 15 x3 * x9  + 79 x4 * x5 + 40 x4 * x6  + 96 x4 * x7  + 38 x4 * x8  + 31 x4 * x9  + 23 x5 * x6 + 9 x5 * x7  + 14 x5 * x8  + 88 x5 * x9  + 20 x6 * x7 + 52 x6 * x8  + 40 x6 * x9  + 84 x7 * x8 + 90 x7 * x9  + 42 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 5
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
