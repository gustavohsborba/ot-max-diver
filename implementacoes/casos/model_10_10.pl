enter pdm
Maximize
[ 19 x0 * x1 + 71 x0 * x2  + 25 x0 * x3  + 10 x0 * x4  + 31 x0 * x5  + 46 x0 * x6  + 13 x0 * x7  + 52 x0 * x8  + 74 x0 * x9  + 19 x1 * x2 + 33 x1 * x3  + 48 x1 * x4  + 44 x1 * x5  + 5 x1 * x6  + 30 x1 * x7  + 77 x1 * x8  + 55 x1 * x9  + 62 x2 * x3 + 13 x2 * x4  + 69 x2 * x5  + 85 x2 * x6  + 91 x2 * x7  + 94 x2 * x8  + 30 x2 * x9  + 83 x3 * x4 + 33 x3 * x5  + 12 x3 * x6  + 91 x3 * x7  + 22 x3 * x8  + 86 x3 * x9  + 93 x4 * x5 + 73 x4 * x6  + 1 x4 * x7  + 99 x4 * x8  + 89 x4 * x9  + 37 x5 * x6 + 36 x5 * x7  + 12 x5 * x8  + 12 x5 * x9  + 69 x6 * x7 + 46 x6 * x8  + 81 x6 * x9  + 31 x7 * x8 + 57 x7 * x9  + 45 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 3
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
