enter pdm
Maximize
[ 60 x0 * x1 + 26 x0 * x2  + 88 x0 * x3  + 62 x0 * x4  + 57 x0 * x5  + 20 x0 * x6  + 41 x0 * x7  + 75 x0 * x8  + 50 x0 * x9  + 48 x1 * x2 + 94 x1 * x3  + 27 x1 * x4  + 18 x1 * x5  + 38 x1 * x6  + 95 x1 * x7  + 51 x1 * x8  + 35 x1 * x9  + 96 x2 * x3 + 31 x2 * x4  + 17 x2 * x5  + 37 x2 * x6  + 62 x2 * x7  + 55 x2 * x8  + 48 x2 * x9  + 68 x3 * x4 + 68 x3 * x5  + 26 x3 * x6  + 56 x3 * x7  + 87 x3 * x8  + 32 x3 * x9  + 12 x4 * x5 + 56 x4 * x6  + 4 x4 * x7  + 76 x4 * x8  + 55 x4 * x9  + 26 x5 * x6 + 78 x5 * x7  + 2 x5 * x8  + 56 x5 * x9  + 77 x6 * x7 + 74 x6 * x8  + 1 x6 * x9  + 22 x7 * x8 + 71 x7 * x9  + 24 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 3
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
