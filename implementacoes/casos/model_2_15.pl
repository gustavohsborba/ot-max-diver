enter pdm
Maximize
[ 42 x0 * x1 + 87 x0 * x2  + 17 x0 * x3  + 97 x0 * x4  + 12 x0 * x5  + 14 x0 * x6  + 100 x0 * x7  + 49 x0 * x8  + 59 x0 * x9  + 81 x0 * x10  + 68 x0 * x11  + 33 x0 * x12  + 82 x0 * x13  + 86 x0 * x14  + 56 x1 * x2 + 24 x1 * x3  + 6 x1 * x4  + 42 x1 * x5  + 10 x1 * x6  + 92 x1 * x7  + 100 x1 * x8  + 7 x1 * x9  + 35 x1 * x10  + 67 x1 * x11  + 18 x1 * x12  + 56 x1 * x13  + 47 x1 * x14  + 49 x2 * x3 + 53 x2 * x4  + 52 x2 * x5  + 11 x2 * x6  + 56 x2 * x7  + 52 x2 * x8  + 67 x2 * x9  + 57 x2 * x10  + 35 x2 * x11  + 26 x2 * x12  + 90 x2 * x13  + 58 x2 * x14  + 11 x3 * x4 + 14 x3 * x5  + 51 x3 * x6  + 80 x3 * x7  + 43 x3 * x8  + 11 x3 * x9  + 44 x3 * x10  + 98 x3 * x11  + 51 x3 * x12  + 29 x3 * x13  + 9 x3 * x14  + 77 x4 * x5 + 40 x4 * x6  + 29 x4 * x7  + 38 x4 * x8  + 18 x4 * x9  + 37 x4 * x10  + 16 x4 * x11  + 81 x4 * x12  + 10 x4 * x13  + 58 x4 * x14  + 59 x5 * x6 + 27 x5 * x7  + 59 x5 * x8  + 30 x5 * x9  + 96 x5 * x10  + 34 x5 * x11  + 95 x5 * x12  + 78 x5 * x13  + 77 x5 * x14  + 31 x6 * x7 + 11 x6 * x8  + 53 x6 * x9  + 2 x6 * x10  + 1 x6 * x11  + 80 x6 * x12  + 67 x6 * x13  + 41 x6 * x14  + 62 x7 * x8 + 21 x7 * x9  + 88 x7 * x10  + 58 x7 * x11  + 33 x7 * x12  + 18 x7 * x13  + 27 x7 * x14  + 95 x8 * x9 + 39 x8 * x10  + 72 x8 * x11  + 68 x8 * x12  + 97 x8 * x13  + 54 x8 * x14  + 57 x9 * x10 + 60 x9 * x11  + 26 x9 * x12  + 34 x9 * x13  + 43 x9 * x14  + 68 x10 * x11 + 13 x10 * x12  + 53 x10 * x13  + 7 x10 * x14  + 2 x11 * x12 + 46 x11 * x13  + 49 x11 * x14  + 49 x12 * x13 + 74 x12 * x14  + 91 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 2
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
