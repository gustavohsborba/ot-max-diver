enter pdm
Maximize
[ 40 x0 * x1 + 12 x0 * x2  + 67 x0 * x3  + 40 x0 * x4  + 93 x0 * x5  + 9 x0 * x6  + 85 x0 * x7  + 63 x0 * x8  + 13 x0 * x9  + 92 x1 * x2 + 42 x1 * x3  + 19 x1 * x4  + 34 x1 * x5  + 4 x1 * x6  + 98 x1 * x7  + 82 x1 * x8  + 11 x1 * x9  + 43 x2 * x3 + 9 x2 * x4  + 91 x2 * x5  + 45 x2 * x6  + 39 x2 * x7  + 27 x2 * x8  + 9 x2 * x9  + 61 x3 * x4 + 18 x3 * x5  + 73 x3 * x6  + 4 x3 * x7  + 54 x3 * x8  + 73 x3 * x9  + 49 x4 * x5 + 17 x4 * x6  + 67 x4 * x7  + 45 x4 * x8  + 23 x4 * x9  + 82 x5 * x6 + 51 x5 * x7  + 70 x5 * x8  + 53 x5 * x9  + 60 x6 * x7 + 20 x6 * x8  + 6 x6 * x9  + 39 x7 * x8 + 14 x7 * x9  + 22 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 3
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
