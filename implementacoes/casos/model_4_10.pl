enter pdm
Maximize
[ 67 x0 * x1 + 49 x0 * x2  + 35 x0 * x3  + 66 x0 * x4  + 83 x0 * x5  + 36 x0 * x6  + 57 x0 * x7  + 71 x0 * x8  + 94 x0 * x9  + 87 x1 * x2 + 15 x1 * x3  + 71 x1 * x4  + 87 x1 * x5  + 94 x1 * x6  + 49 x1 * x7  + 50 x1 * x8  + 41 x1 * x9  + 6 x2 * x3 + 42 x2 * x4  + 87 x2 * x5  + 81 x2 * x6  + 98 x2 * x7  + 51 x2 * x8  + 35 x2 * x9  + 79 x3 * x4 + 89 x3 * x5  + 4 x3 * x6  + 76 x3 * x7  + 90 x3 * x8  + 35 x3 * x9  + 76 x4 * x5 + 89 x4 * x6  + 64 x4 * x7  + 98 x4 * x8  + 19 x4 * x9  + 45 x5 * x6 + 93 x5 * x7  + 37 x5 * x8  + 69 x5 * x9  + 92 x6 * x7 + 24 x6 * x8  + 73 x6 * x9  + 65 x7 * x8 + 57 x7 * x9  + 9 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 5
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
