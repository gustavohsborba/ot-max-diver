enter pdm
Maximize
[ 46 x0 * x1 + 35 x0 * x2  + 15 x0 * x3  + 2 x0 * x4  + 10 x0 * x5  + 51 x0 * x6  + 11 x0 * x7  + 60 x0 * x8  + 29 x0 * x9  + 81 x0 * x10  + 96 x0 * x11  + 40 x0 * x12  + 66 x0 * x13  + 17 x0 * x14  + 5 x1 * x2 + 21 x1 * x3  + 8 x1 * x4  + 10 x1 * x5  + 46 x1 * x6  + 14 x1 * x7  + 54 x1 * x8  + 72 x1 * x9  + 76 x1 * x10  + 65 x1 * x11  + 97 x1 * x12  + 44 x1 * x13  + 10 x1 * x14  + 31 x2 * x3 + 15 x2 * x4  + 70 x2 * x5  + 37 x2 * x6  + 28 x2 * x7  + 43 x2 * x8  + 68 x2 * x9  + 37 x2 * x10  + 35 x2 * x11  + 73 x2 * x12  + 96 x2 * x13  + 64 x2 * x14  + 34 x3 * x4 + 63 x3 * x5  + 83 x3 * x6  + 24 x3 * x7  + 5 x3 * x8  + 43 x3 * x9  + 17 x3 * x10  + 25 x3 * x11  + 59 x3 * x12  + 71 x3 * x13  + 96 x3 * x14  + 77 x4 * x5 + 17 x4 * x6  + 95 x4 * x7  + 88 x4 * x8  + 57 x4 * x9  + 10 x4 * x10  + 76 x4 * x11  + 92 x4 * x12  + 80 x4 * x13  + 78 x4 * x14  + 16 x5 * x6 + 72 x5 * x7  + 99 x5 * x8  + 77 x5 * x9  + 5 x5 * x10  + 40 x5 * x11  + 82 x5 * x12  + 68 x5 * x13  + 18 x5 * x14  + 30 x6 * x7 + 71 x6 * x8  + 5 x6 * x9  + 84 x6 * x10  + 19 x6 * x11  + 88 x6 * x12  + 41 x6 * x13  + 87 x6 * x14  + 61 x7 * x8 + 26 x7 * x9  + 72 x7 * x10  + 32 x7 * x11  + 96 x7 * x12  + 43 x7 * x13  + 75 x7 * x14  + 91 x8 * x9 + 19 x8 * x10  + 28 x8 * x11  + 7 x8 * x12  + 63 x8 * x13  + 42 x8 * x14  + 12 x9 * x10 + 17 x9 * x11  + 30 x9 * x12  + 57 x9 * x13  + 45 x9 * x14  + 2 x10 * x11 + 28 x10 * x12  + 77 x10 * x13  + 66 x10 * x14  + 35 x11 * x12 + 39 x11 * x13  + 23 x11 * x14  + 33 x12 * x13 + 84 x12 * x14  + 13 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 4
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
