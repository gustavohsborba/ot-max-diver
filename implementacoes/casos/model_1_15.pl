enter pdm
Maximize
[ 98 x0 * x1 + 46 x0 * x2  + 92 x0 * x3  + 59 x0 * x4  + 69 x0 * x5  + 58 x0 * x6  + 15 x0 * x7  + 43 x0 * x8  + 6 x0 * x9  + 39 x0 * x10  + 84 x0 * x11  + 24 x0 * x12  + 22 x0 * x13  + 86 x0 * x14  + 83 x1 * x2 + 17 x1 * x3  + 26 x1 * x4  + 75 x1 * x5  + 83 x1 * x6  + 67 x1 * x7  + 58 x1 * x8  + 94 x1 * x9  + 45 x1 * x10  + 15 x1 * x11  + 59 x1 * x12  + 16 x1 * x13  + 30 x1 * x14  + 43 x2 * x3 + 23 x2 * x4  + 9 x2 * x5  + 96 x2 * x6  + 41 x2 * x7  + 29 x2 * x8  + 1 x2 * x9  + 81 x2 * x10  + 32 x2 * x11  + 30 x2 * x12  + 8 x2 * x13  + 71 x2 * x14  + 82 x3 * x4 + 79 x3 * x5  + 76 x3 * x6  + 49 x3 * x7  + 41 x3 * x8  + 28 x3 * x9  + 34 x3 * x10  + 59 x3 * x11  + 24 x3 * x12  + 76 x3 * x13  + 34 x3 * x14  + 33 x4 * x5 + 23 x4 * x6  + 95 x4 * x7  + 56 x4 * x8  + 27 x4 * x9  + 21 x4 * x10  + 9 x4 * x11  + 38 x4 * x12  + 17 x4 * x13  + 83 x4 * x14  + 16 x5 * x6 + 39 x5 * x7  + 63 x5 * x8  + 69 x5 * x9  + 3 x5 * x10  + 42 x5 * x11  + 56 x5 * x12  + 94 x5 * x13  + 54 x5 * x14  + 76 x6 * x7 + 84 x6 * x8  + 55 x6 * x9  + 2 x6 * x10  + 39 x6 * x11  + 44 x6 * x12  + 96 x6 * x13  + 9 x6 * x14  + 24 x7 * x8 + 36 x7 * x9  + 35 x7 * x10  + 49 x7 * x11  + 92 x7 * x12  + 62 x7 * x13  + 41 x7 * x14  + 47 x8 * x9 + 21 x8 * x10  + 49 x8 * x11  + 43 x8 * x12  + 29 x8 * x13  + 35 x8 * x14  + 94 x9 * x10 + 2 x9 * x11  + 42 x9 * x12  + 62 x9 * x13  + 32 x9 * x14  + 84 x10 * x11 + 30 x10 * x12  + 93 x10 * x13  + 79 x10 * x14  + 88 x11 * x12 + 40 x11 * x13  + 43 x11 * x14  + 70 x12 * x13 + 75 x12 * x14  + 22 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 6
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
