enter pdm
Maximize
[ 86 x0 * x1 + 45 x0 * x2  + 4 x0 * x3  + 11 x0 * x4  + 30 x0 * x5  + 68 x0 * x6  + 29 x0 * x7  + 45 x0 * x8  + 52 x0 * x9  + 42 x1 * x2 + 8 x1 * x3  + 35 x1 * x4  + 93 x1 * x5  + 47 x1 * x6  + 68 x1 * x7  + 81 x1 * x8  + 96 x1 * x9  + 50 x2 * x3 + 10 x2 * x4  + 90 x2 * x5  + 75 x2 * x6  + 53 x2 * x7  + 9 x2 * x8  + 71 x2 * x9  + 57 x3 * x4 + 33 x3 * x5  + 34 x3 * x6  + 20 x3 * x7  + 30 x3 * x8  + 61 x3 * x9  + 12 x4 * x5 + 98 x4 * x6  + 88 x4 * x7  + 77 x4 * x8  + 4 x4 * x9  + 14 x5 * x6 + 75 x5 * x7  + 2 x5 * x8  + 100 x5 * x9  + 8 x6 * x7 + 86 x6 * x8  + 53 x6 * x9  + 85 x7 * x8 + 93 x7 * x9  + 33 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 5
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
