enter pdm
Maximize
[ 30 x0 * x1 + 17 x0 * x2  + 37 x0 * x3  + 22 x0 * x4  + 14 x0 * x5  + 82 x0 * x6  + 1 x0 * x7  + 63 x0 * x8  + 31 x0 * x9  + 92 x0 * x10  + 15 x0 * x11  + 12 x0 * x12  + 39 x0 * x13  + 52 x0 * x14  + 45 x1 * x2 + 21 x1 * x3  + 8 x1 * x4  + 18 x1 * x5  + 8 x1 * x6  + 81 x1 * x7  + 58 x1 * x8  + 53 x1 * x9  + 62 x1 * x10  + 71 x1 * x11  + 12 x1 * x12  + 83 x1 * x13  + 23 x1 * x14  + 82 x2 * x3 + 93 x2 * x4  + 87 x2 * x5  + 15 x2 * x6  + 27 x2 * x7  + 54 x2 * x8  + 6 x2 * x9  + 4 x2 * x10  + 82 x2 * x11  + 11 x2 * x12  + 23 x2 * x13  + 90 x2 * x14  + 91 x3 * x4 + 25 x3 * x5  + 61 x3 * x6  + 90 x3 * x7  + 44 x3 * x8  + 24 x3 * x9  + 58 x3 * x10  + 27 x3 * x11  + 18 x3 * x12  + 8 x3 * x13  + 69 x3 * x14  + 100 x4 * x5 + 14 x4 * x6  + 22 x4 * x7  + 75 x4 * x8  + 95 x4 * x9  + 74 x4 * x10  + 33 x4 * x11  + 87 x4 * x12  + 18 x4 * x13  + 3 x4 * x14  + 27 x5 * x6 + 69 x5 * x7  + 50 x5 * x8  + 20 x5 * x9  + 17 x5 * x10  + 40 x5 * x11  + 5 x5 * x12  + 100 x5 * x13  + 48 x5 * x14  + 13 x6 * x7 + 80 x6 * x8  + 93 x6 * x9  + 36 x6 * x10  + 32 x6 * x11  + 89 x6 * x12  + 57 x6 * x13  + 32 x6 * x14  + 47 x7 * x8 + 81 x7 * x9  + 26 x7 * x10  + 28 x7 * x11  + 31 x7 * x12  + 79 x7 * x13  + 66 x7 * x14  + 96 x8 * x9 + 50 x8 * x10  + 66 x8 * x11  + 37 x8 * x12  + 14 x8 * x13  + 58 x8 * x14  + 62 x9 * x10 + 81 x9 * x11  + 84 x9 * x12  + 79 x9 * x13  + 1 x9 * x14  + 7 x10 * x11 + 74 x10 * x12  + 13 x10 * x13  + 4 x10 * x14  + 50 x11 * x12 + 40 x11 * x13  + 14 x11 * x14  + 63 x12 * x13 + 89 x12 * x14  + 34 x13 * x14] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10 + x11 + x12 + x13 + x14 = 9
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14
End
optimize
display solution variables x0-x14
display solution objective
