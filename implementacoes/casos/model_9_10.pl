enter pdm
Maximize
[ 92 x0 * x1 + 3 x0 * x2  + 35 x0 * x3  + 74 x0 * x4  + 8 x0 * x5  + 97 x0 * x6  + 71 x0 * x7  + 20 x0 * x8  + 98 x0 * x9  + 16 x1 * x2 + 48 x1 * x3  + 14 x1 * x4  + 55 x1 * x5  + 31 x1 * x6  + 56 x1 * x7  + 65 x1 * x8  + 71 x1 * x9  + 53 x2 * x3 + 56 x2 * x4  + 84 x2 * x5  + 36 x2 * x6  + 26 x2 * x7  + 79 x2 * x8  + 80 x2 * x9  + 15 x3 * x4 + 70 x3 * x5  + 30 x3 * x6  + 86 x3 * x7  + 11 x3 * x8  + 53 x3 * x9  + 65 x4 * x5 + 54 x4 * x6  + 84 x4 * x7  + 67 x4 * x8  + 37 x4 * x9  + 37 x5 * x6 + 58 x5 * x7  + 46 x5 * x8  + 87 x5 * x9  + 87 x6 * x7 + 97 x6 * x8  + 59 x6 * x9  + 14 x7 * x8 + 20 x7 * x9  + 56 x8 * x9] / 2
Subject To
x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 = 3
binary
 x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
End
optimize
display solution variables x0-x9
display solution objective
