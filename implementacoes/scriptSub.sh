#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --mail-type=END
#SBATCH --mail-user=felipeduarte@lsi.cefetmg.br
#SBATCH --partition=mmc_3x64
module load cplex

srun cplex -f $1
