#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char *argv[]) {

    if(argc != 2){
        cout << "usage: " << argv[0] << " dim" << endl
             << "Generate Dataset for Diversity Problem, that is," << endl
             << "size of problem, no. of clusters, and a symmetric "<< endl
             << "matrix that has its principal diagonal equals 0" << endl;
    }

    int dim = atoi(argv[1]);

    int** mat = new int*[dim];
    for(int i=0; i<dim; i++)
        mat[i] = new int[dim];


    srand(time(0));
    for(int i=0; i<dim; i++){
        for(int j=0; j<i; j++){
            mat[i][j] = rand()%100 + 1;
            mat[j][i] = mat[i][j];
        }
        mat[i][i] = 0;
    }

    cout << dim << " " << (int) (rand()%dim/2) + 2 << endl;

    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            cout << mat[i][j] << " ";
        }
        cout << endl;
    }

    for(size_t i = 0; i < dim; ++i)
        delete mat[i];

    return 0;
}