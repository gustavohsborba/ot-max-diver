#include <bits/stdc++.h>
#define IT    1000
#define ALPHA 0
#define INF   0x3f3f3f3f

using namespace std;

int numVertices,numElemSol;
vector< vector<int> > G;
vector<int> solInit,solLocal,melhorSol;
int vSolInit,vSolLocal,vMelhorSol;

typedef struct arestaS{
    int u,v,w;
} aresta;

typedef struct verticeS{
    int v,eval;
} vertice;

void constroiSolucao(){
    int i,j,k,v;
    vector<aresta> arestas;
    vector<vertice> vertices;
    vector<int> used,verticesC;
    aresta a;

    used.assign(G.size(),0);
    solInit.clear();

    if(ALPHA){
        a.w=-1;
        for(i=0;i<G.size();i++){
            for(j=0;j<G[i].size();j++){
                if(G[i][j] > a.w){
                    a.u=i;
                    a.v=j;
                    a.w=G[i][j];
                }
            }
        }
    }else{
        arestas.clear();
        for(i=0;i<G.size();i++){
            for(j=0;j<G[i].size();j++){
                a.u=i;
                a.v=j;
                a.w=G[i][j];
                arestas.push_back(a);
            }
        }

        a=arestas[rand()%arestas.size()];
    }
    used[a.u]=used[a.v]=1;
    solInit.push_back(a.u);
    solInit.push_back(a.v);

    for(i=0;i<numElemSol-2;i++){
        int maxEvalV=-1;
        
        vertices.clear();
        verticesC.clear();
        for(k=0;k<used.size();k++){
            if(!used[k]){
                vertice v;
                v.v=k;
                v.eval=0;

                for(j=0;j<solInit.size();j++){
                    v.eval+=G[k][solInit[j]];
                }

                maxEvalV=max(v.eval,maxEvalV);
                vertices.push_back(v);
            }
        }

        //filtra os vertices com maior indice
        for(k=0;k<vertices.size();k++){
            if(vertices[k].eval == maxEvalV)
                verticesC.push_back(vertices[k].v);
        }

        if(ALPHA){
            v=verticesC[rand()%verticesC.size()];
        }else{
            vertice vs;
            vs=vertices[rand()%vertices.size()];
            v=vs.v;
        }

        //adiciona vertice a solucao
        solInit.push_back(v);
        used[v]=1;
    }
}

int calculaDiversidade(vector<int> v){
    int i,j;
    int diver;
    set<int> used;

    for(i=0;i<v.size();i++){
        used.insert(v[i]);
    }

    diver=0;
    for(i=0;i<v.size();i++){
        for(j=i+1;j<v.size();j++){
            diver+=G[v[i]][v[j]];
        }
    }
    
    return diver;
}

void buscaLocal(){
    vector<int> solucao;
    set<int> used;
    int evalSolucao;
    int i,j;

    solLocal=solucao=solInit;
    vSolLocal=evalSolucao=vSolInit;

    for(i=0;i<solucao.size();i++){
        used.insert(solucao[i]);
    }

    for(i=0;i<solucao.size();i++){
        int retirado=solucao[i];
        for(j=0;j<G.size();j++){
            if(used.find(j) == used.end()){
                solucao[i]=j;
                evalSolucao=calculaDiversidade(solucao);
                if(evalSolucao > vSolLocal){
                    solLocal=solucao;
                    vSolLocal=evalSolucao;
                }
            }
        }
        solucao[i]=retirado;
    }
}

int main(){
    int i,j,num,it,fo;

    srand(time(NULL));

    clock_t ini = clock();

    scanf("%d %d", &numVertices, &numElemSol);

    G.clear();

    for(i=0;i<numVertices;i++){
        vector<int> arestas;
        for(j=0;j<numVertices;j++){
            scanf("%d",&num);
            arestas.push_back(num);
        }
        G.push_back(arestas);
    }

    it=0;
    vMelhorSol=-1;
    while(++it < IT){
        constroiSolucao();
        vSolInit=calculaDiversidade(solInit);
        buscaLocal();
        vSolLocal=calculaDiversidade(solLocal);
        if(vSolLocal > vMelhorSol){
            melhorSol=solLocal;
            vMelhorSol=vSolLocal;
        }
    }

    sort(melhorSol.begin(),melhorSol.end());

    fo=0;
    for(i=0;i<melhorSol.size();i++){
        for(j=i+1;j<melhorSol.size();j++){
            fo+=G[melhorSol[i]][melhorSol[j]];
        }
    }

    clock_t fim = clock();

    //printa melhor sol
    cout << "m=" << numVertices << ", n=" << numElemSol << endl;
    cout << "fo = " << fo/2.0 << endl;
    for(i=0;i<melhorSol.size();i++){
        cout << melhorSol[i] << " ";
    }
    cout << endl << "Elapsed time: " << (double)(fim - ini)/(double)CLOCKS_PER_SEC << endl;

    return 0;
}
